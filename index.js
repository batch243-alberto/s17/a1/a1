/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.

*/
	
	//first function here:

	function yourInformation(){
		let yourName = prompt("Your Name");
		let yourAge = prompt("Your Age");
		let yourLocation = prompt("Your location");

		console.log("Hello, " + yourName);
		console.log("You are " + yourAge + " years old.");
		console.log("You live in " + yourLocation);
	}

	yourInformation()

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	function yourTopBand(){
		let band1 = ("December Avenue");
		let band2 = ("Ben & Ben");
		let band3 = ("Silent Sanctuary");
		let band4 = ("Spongecola");
		let band5 = ("Rivermaya");

		console.log("1. " + band1);
		console.log("2. " + band2);
		console.log("3. " + band3);
		console.log("4. " + band4);
		console.log("5. " + band5);
	};

	yourTopBand();
	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	function yourFavMovie(){
		let movie1 = ("Black Panther");
		let rating1 = ("96%");
		let movie2 = ("Avenger: Endgame");
		let rating2 = ("94%");
		let movie3 = ("Iron Man");
		let rating3 = ("94%");
		let movie4 = ("Thor: Ragnarok");
		let rating4 = ("93%");
		let movie5 = ("Spider-man: No Way Home");
		let rating5 = ("93%");

		console.log("1. " + movie1);
		console.log("Rotten Tomatoes Rating: " + rating1);
		console.log("2. " + movie2);
		console.log("Rotten Tomatoes Rating: " + rating2);
		console.log("3. " + movie3);
		console.log("Rotten Tomatoes Rating: " + rating3);
		console.log("4. " + movie4);
		console.log("Rotten Tomatoes Rating: " + rating4);
		console.log("5. " + movie5);
		console.log("Rotten Tomatoes Rating: " + rating5);

	};

	yourFavMovie();
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


    let printFriends = function printUsers(){
		alert("Hi! Please add the names of your friends.");
		let friend1 = prompt("Enter your first friend's name:"); 
		let friend2 = prompt("Enter your second friend's name:"); 
		let friend3 = prompt("Enter your third friend's name:");

		console.log("You are friends with:")
		console.log(friend1); 
		console.log(friend2); 
		console.log(friend3); 
	};

	printFriends();


